import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading-button',
  templateUrl: './loading-button.component.html',
  styleUrls: ['./loading-button.component.css']
})
export class LoadingButtonComponent implements OnInit {

  @Input() typeB: string = "submit"
  @Input() text: string = ""
  @Input() loading: boolean = false
  @Input() valid: boolean = false
  @Input() className: string = ""

  constructor() { }

  ngOnInit(): void {
  }

}
