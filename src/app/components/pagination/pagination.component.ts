import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  @Input() pageable: any = {}

  @Output() nextPageEvent = new EventEmitter();
  @Output() backPageEvent = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  next() {
    this.nextPageEvent.emit()
  }
  back()  {
    this.backPageEvent.emit()
  }

}
