import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.css']
})
export class ProjectFormComponent implements OnInit {

  @Input() formType: string = ""
  @Input() loading: any = {}
  @Input() projectFb: FormGroup = this.fb.group({})
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() saveEvent = new EventEmitter<any>();
  @Output() updateEvent = new EventEmitter<any>();

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
  }


  close() {
    this.projectFb.reset()
    this.closeEvent.emit(false);
  }

  submit() {
    this.formType === 'create' ? this.save() : this.update()
  }
  save() {
    this.saveEvent.emit();
  }
  update() {
    this.updateEvent.emit();
  }

}
