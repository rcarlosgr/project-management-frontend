import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent implements OnInit {

  @Input() formType: string = ""
  @Input() loading: any = {}
  @Input() taskFb: FormGroup = this.fb.group({})
  @Input() taskStates: any
  @Output() closeEvent = new EventEmitter<boolean>();
  @Output() saveEvent = new EventEmitter<any>();
  @Output() updateEvent = new EventEmitter<any>();

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
  }


  close() {
    this.taskFb.reset()
    this.closeEvent.emit(false);
  }

  submit() {
    this.formType === 'create' ? this.save() : this.update()
  }
  save() {
    this.saveEvent.emit();
  }
  update() {
    this.updateEvent.emit();
  }

}
