import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import queryString from 'query-string';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  findAll(page: number, filters: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/users/paginated?page=${page}&${queryString.stringify(filters)}`);
  }
  getByEmail(email: string): Observable<any>{
    return this.http.get<any>(`${this.apiUrl}/users/get-by-email/${email}`);
  }
  save(user: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/users`, user);
  }
  update(id: number, user: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/users/${id}`, user);
  }
  destroy(id: number): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/users/${id}`);
  }
}
