import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProjectService } from 'src/app/services/project.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  projects: any[] = []
  user: any = null
  formIsOpen: boolean = false
  formType: string = "create"
  loading: any = {
    button: false,
  }
  page: number = 0
  filters: any = {
    search: "",
  }
  pageable: any = null

  projectFb = this.fb.group({
    id: [""],
    name: ["", [Validators.required]],
    description: ["", [Validators.required]],
    startDate: [null, [Validators.required]],
    endDate: [null, [Validators.required]],
  })

  constructor(
    private fb: FormBuilder,
    private projectService: ProjectService,
  ) { }

  ngOnInit(): void {
    this.getAll()
  }

  changeForm(state: boolean) {
    this.formIsOpen = state
  }

  nextPage() {
    this.page++
    this.getAll()
  }
  backPage(){
    this.page--
    this.getAll()
  }

  filter() {
    this.page = 0
    this.filters.search = this.filters.search.trim()
    this.getAll()
  }
  reset() {
    this.page = 0
    this.filters.search = ""
    this.filters.roleId = ""
    this.getAll()
  }

  getAll() {
    this.projectService.findAll(this.page, this.filters).subscribe({
      next: res => {
        this.projects = res.content
        this.pageable = {
          totalPages: res.totalPages,
          totalElements: res.totalElements,
          number: res.number,
          first: res.first,
          last: res.last,
        }
      },
      error: err => {
        Swal.fire("", "Ocurrió un problema", "warning")
      }
    })
  }

  create() {
    this.formType = "create"
    this.changeForm(true)
  }
  save() {
    if (this.projectFb.invalid) {
      Swal.fire("", "Datos incorrectos", "warning")
      return
    }

    this.loading.button = true
    this.projectService.save(this.projectFb.value).subscribe({
      next: res => {
        this.loading.button = false
        this.getAll()
        this.changeForm(false)
        this.projectFb.reset()
        Swal.fire("", "Registrado con éxito", "success")
      },
      error: err => {
        this.loading.button = false
        if (err.status == 422) {
          const errors: string[] = Object.values(err.error);
          errors.forEach((el: string) => Swal.fire("", el, "warning"))
        } else {
          Swal.fire("", "Ocurrió un problema", "warning")
        }
      },
    })
  }
  edit(item: any) {
    this.projectFb.patchValue({
      id: item.id,
      name: item.name,
      description: item.description,
      startDate: item.startDate,
      endDate: item.endDate,
    })
    this.formType = "edit"
    this.changeForm(true)
  }
  update() {
    if (this.projectFb.invalid) {
      Swal.fire("", "Datos incorrectos", "warning")
      return
    }
    this.loading.button = true
    this.projectService.update(Number(this.projectFb.value.id), this.projectFb.value).subscribe({
      next: res => {
        this.loading.button = false
        this.getAll()
        this.changeForm(false)
        this.projectFb.reset()
        Swal.fire("", "Actualizado con éxito", "success")
      },
      error: err => {
        this.loading.button = false
        if (err.status == 422) {
          const errors: string[] = Object.values(err.error);
          errors.forEach((el: string) => Swal.fire("", el, "warning"))
        } else {
          Swal.fire("", "Ocurrió un problema", "warning")
        }
      },
    })
  }

  destroy(id: number) {
    Swal.fire({
      title: 'Estas seguro?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, bórralo!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.projectService.destroy(id).subscribe({
          next: res => {
            this.page = 0
            this.getAll()
            this.changeForm(false)
            Swal.fire("", "Eliminado con éxito", "success")
          },
          error: err => {
            if (err.status == 422) {
              const errors: string[] = Object.values(err.error);
              errors.forEach((el: string) => Swal.fire("", el, "warning"))
            } else {
              Swal.fire("", "Ocurrió un problema", "warning")
            }
          },
        })
      }
    })
  }

}
