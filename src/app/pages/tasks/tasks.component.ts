import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { TASK_STATES } from 'src/utils/constants.util';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  tasks: any[] = []
  task: any = null
  formIsOpen: boolean = false
  formType: string = "create"
  loading: any = {
    button: false,
  }
  page: number = 0
  filters: any = {
    search: "",
  }
  pageable: any = null

  taskFb = this.fb.group({
    id: [""],
    title: ["", [Validators.required]],
    description: ["", [Validators.required]],
    state: ["", [Validators.required]],
    startDate: [null, [Validators.required]],
    endDate: [null, [Validators.required]],
    projectId: ["", [Validators.required]],
  })

  taskStates = TASK_STATES

  constructor(
    private fb: FormBuilder,
    private taskService: TaskService,
  ) { }

  ngOnInit(): void {
    this.getAll()
  }

  changeForm(state: boolean) {
    this.formIsOpen = state
  }

  nextPage() {
    this.page++
    this.getAll()
  }
  backPage(){
    this.page--
    this.getAll()
  }

  filter() {
    this.page = 0
    this.filters.search = this.filters.search.trim()
    this.getAll()
  }
  reset() {
    this.page = 0
    this.filters.search = ""
    this.filters.roleId = ""
    this.getAll()
  }

  getAll() {
    this.taskService.findAll(this.page, this.filters).subscribe({
      next: res => {
        this.tasks = res.content
        this.pageable = {
          totalPages: res.totalPages,
          totalElements: res.totalElements,
          number: res.number,
          first: res.first,
          last: res.last,
        }
      },
      error: err => {
        Swal.fire("", "Ocurrió un problema", "warning")
      }
    })
  }

  create() {
    this.formType = "create"
    this.changeForm(true)
  }
  save() {
    if (this.taskFb.invalid) {
      Swal.fire("", "Datos incorrectos", "warning")
      return
    }

    this.loading.button = true
    this.taskService.save(this.taskFb.value).subscribe({
      next: res => {
        this.loading.button = false
        this.getAll()
        this.changeForm(false)
        this.taskFb.reset()
        Swal.fire("", "Registrado con éxito", "success")
      },
      error: err => {
        this.loading.button = false
        if (err.status == 422) {
          const errors: string[] = Object.values(err.error);
          errors.forEach((el: string) => Swal.fire("", el, "warning"))
        } else {
          Swal.fire("", "Ocurrió un problema", "warning")
        }
      },
    })
  }

  edit(item: any) {
    this.taskFb.patchValue({
      id: item.id,
      title: item.title,
      description: item.description,
      state: item.state,
      startDate: item.startDate,
      endDate: item.endDate,
      projectId: item.project.id,
    })
    this.formType = "edit"
    this.changeForm(true)
  }
  update() {
    if (this.taskFb.invalid) {
      Swal.fire("", "Datos incorrectos", "warning")
      return
    }
    this.loading.button = true
    this.taskService.update(Number(this.taskFb.value.id), this.taskFb.value).subscribe({
      next: res => {
        this.loading.button = false
        this.getAll()
        this.changeForm(false)
        this.taskFb.reset()
        Swal.fire("", "Actualizado con éxito", "success")
      },
      error: err => {
        this.loading.button = false
        if (err.status == 422) {
          const errors: string[] = Object.values(err.error);
          errors.forEach((el: string) => Swal.fire("", el, "warning"))
        } else {
          Swal.fire("", "Ocurrió un problema", "warning")
        }
      },
    })
  }

  destroy(id: number) {
    Swal.fire({
      title: 'Estas seguro?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, bórralo!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.taskService.destroy(id).subscribe({
          next: res => {
            this.page = 0
            this.getAll()
            this.changeForm(false)
            Swal.fire("", "Eliminado con éxito", "success")
          },
          error: err => {
            if (err.status == 422) {
              const errors: string[] = Object.values(err.error);
              errors.forEach((el: string) => Swal.fire("", el, "warning"))
            } else {
              Swal.fire("", "Ocurrió un problema", "warning")
            }
          },
        })
      }
    })
  }

}
