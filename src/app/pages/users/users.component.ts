import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: any[] = []
  user: any = null
  formIsOpen: boolean = false
  formType: string = "create"
  loading: any = {
    button: false,
  }
  page: number = 0
  filters: any = {
    search: "",
  }
  pageable: any = null

  userFb = this.fb.group({
    id: [""],
    name: ["", [Validators.required]],
    email: ["", [Validators.required, Validators.email]],
  })

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.getAll()
  }

  changeForm(state: boolean) {
    this.formIsOpen = state
  }

  nextPage() {
    this.page++
    this.getAll()
  }
  backPage(){
    this.page--
    this.getAll()
  }

  filter() {
    this.page = 0
    this.filters.search = this.filters.search.trim()
    this.getAll()
  }
  reset() {
    this.page = 0
    this.filters.search = ""
    this.filters.roleId = ""
    this.getAll()
  }

  getAll() {
    this.userService.findAll(this.page, this.filters).subscribe({
      next: res => {
        this.users = res.content
        this.pageable = {
          totalPages: res.totalPages,
          totalElements: res.totalElements,
          number: res.number,
          first: res.first,
          last: res.last,
        }
      },
      error: err => {
        Swal.fire("", "Ocurrió un problema", "warning")
      }
    })
  }

  create() {
    this.formType = "create"
    this.changeForm(true)
  }
  save() {
    if (this.userFb.invalid) {
      Swal.fire("", "Datos incorrectos", "warning")
      return
    }

    this.loading.button = true
    this.userService.save(this.userFb.value).subscribe({
      next: res => {
        this.loading.button = false
        this.getAll()
        this.changeForm(false)
        this.userFb.reset()
        Swal.fire("", "Registrado con éxito", "success")
      },
      error: err => {
        this.loading.button = false
        if (err.status == 422) {
          const errors: string[] = Object.values(err.error);
          errors.forEach((el: string) => Swal.fire("", el, "warning"))
        } else {
          Swal.fire("", "Ocurrió un problema", "warning")
        }
      },
    })
  }
  edit(item: any) {
    this.userFb.patchValue({
      id: item.id,
      name: item.name,
      email: item.email,
    })
    this.formType = "edit"
    this.changeForm(true)
  }
  update() {
    if (this.userFb.invalid) {
      Swal.fire("", "Datos incorrectos", "warning")
      return
    }
    this.loading.button = true
    this.userService.update(Number(this.userFb.value.id), this.userFb.value).subscribe({
      next: res => {
        this.loading.button = false
        this.getAll()
        this.changeForm(false)
        this.userFb.reset()
        Swal.fire("", "Actualizado con éxito", "success")
      },
      error: err => {
        this.loading.button = false
        if (err.status == 422) {
          const errors: string[] = Object.values(err.error);
          errors.forEach((el: string) => Swal.fire("", el, "warning"))
        } else {
          Swal.fire("", "Ocurrió un problema", "warning")
        }
      },
    })
  }

  destroy(id: number) {
    Swal.fire({
      title: 'Estas seguro?',
      text: "¡No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '¡Sí, bórralo!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.userService.destroy(id).subscribe({
          next: res => {
            this.page = 0
            this.getAll()
            this.changeForm(false)
            Swal.fire("", "Eliminado con éxito", "success")
          },
          error: err => {
            if (err.status == 422) {
              const errors: string[] = Object.values(err.error);
              errors.forEach((el: string) => Swal.fire("", el, "warning"))
            } else {
              Swal.fire("", "Ocurrió un problema", "warning")
            }
          },
        })
      }
    })
  }

}
