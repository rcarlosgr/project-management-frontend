import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './pages/users/users.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { UserFormComponent } from './components/users/user-form/user-form.component';
import { LoadingButtonComponent } from './components/loading-button/loading-button.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectsComponent } from './pages/projects/projects.component';
import { ProjectFormComponent } from './components/projects/project-form/project-form.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import { TaskFormComponent } from './components/tasks/task-form/task-form.component';
import { HomeComponent } from './pages/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PaginationComponent,
    UserFormComponent,
    LoadingButtonComponent,
    ProjectsComponent,
    ProjectFormComponent,
    TasksComponent,
    TaskFormComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
